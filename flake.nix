{
  description = "Mike's Nix configs";

  inputs = {
    flake-schemas.url = "github:DeterminateSystems/flake-schemas";
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-24.11";
    nixpkgs-unstable.url = "github:nixos/nixpkgs?ref=nixos-unstable";
    chaotic.url = "github:chaotic-cx/nyx?ref=nyxpkgs-unstable";
    nixos-hardware.url = "github:NixOS/nixos-hardware?ref=master";

    home-manager = {
      url = "github:nix-community/home-manager?ref=release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    plasma-manager = {
      url = "github:nix-community/plasma-manager";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.home-manager.follows = "home-manager";
    };

    declarative-flatpak = {
      url = "github:GermanBread/declarative-flatpak?ref=stable-v3";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.home-manager.follows = "home-manager";
    };

    nix-darwin = {
      url = "github:LnL7/nix-darwin?ref=nix-darwin-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # tmp fix until https://github.com/LnL7/nix-darwin/pull/942 is merged
    tmp-nh-darwin-pr = {
      url = "github:LnL7/nix-darwin?ref=refs/pull/942/merge";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    # tmp until nh 4.0 is released
    tmp-nh-master = {
      url = "github:viperML/nh";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    mac-app-util.url = "github:hraban/mac-app-util";

    nix-on-droid = {
      # TODO: upgrade to 24.11 once released
      url = "github:nix-community/nix-on-droid/release-24.05";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.home-manager.follows = "home-manager";
    };

    lanzaboote = {
      url = "github:nix-community/lanzaboote?ref=v0.4.1";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nix-index-database = {
      url = "github:nix-community/nix-index-database";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nix-homebrew = {
      url = "github:zhaofengli/nix-homebrew";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
      inputs.nix-darwin.follows = "nix-darwin";
    };

    homebrew-core = {
      url = "github:homebrew/homebrew-core";
      flake = false;
    };
    homebrew-cask = {
      url = "github:homebrew/homebrew-cask";
      flake = false;
    };
    homebrew-bundle = {
      url = "github:homebrew/homebrew-bundle";
      flake = false;
    };
  };

  outputs =
    { self
    , nixpkgs
    , nixpkgs-unstable
    , nix-darwin
    , nix-on-droid
    , flake-schemas
    , ...
    }@inputs:
    {
      schemas = flake-schemas.schemas;

      lib = import ./flake-helpers self;

      nixosConfigurations = {
        optimism = nixpkgs.lib.nixosSystem rec {
          system = "x86_64-linux";
          modules = [
            ./nixos
            ./nixos/hosts/optimism.nix
            ./nixos/install-specific/optimism.nix
          ];
          specialArgs = {
            flake = self;
            inherit inputs;
            unstable = import nixpkgs-unstable {
              inherit system;
              config.allowUnfree = true;
            };
          };
        };
      };

      darwinConfigurations = {
        "miklostoth-mbp" = nix-darwin.lib.darwinSystem {
          modules = [
            { nixpkgs.hostPlatform = "aarch64-darwin"; }
            ./darwin
          ];
          specialArgs = {
            flake = self;
            inherit inputs;
            unstable = import nixpkgs-unstable {
              system = "aarch64-darwin";
              config.allowUnfree = true;
            };
          };
        };
      };

      nixOnDroidConfigurations = {
        default = nix-on-droid.lib.nixOnDroidConfiguration {
          pkgs = import nixpkgs {
            system = "aarch64-linux";
            config.allowUnfree = true;
          };
          modules = [ ./nix-on-droid ];
          extraSpecialArgs = {
            flake = self;
            inherit inputs;
            unstable = import nixpkgs-unstable {
              system = "aarch64-linux";
              config.allowUnfree = true;
            };
          };
        };
      };

      homeConfigurations = {
        amd64-linux = self.lib.generateHMConfig {
          system = "x86_64-linux";
          osImport = ./home-manager/os/linux;
          userName = "mike";
          homeDir = "/home/mike";
        };
        aarch64-darwin = self.lib.generateHMConfig {
          system = "aarch64-darwin";
          osImport = ./home-manager/os/darwin;
          userName = "miklos.toth";
          homeDir = "/Users/miklos.toth";
        };
      };

      packages =
        nixpkgs.lib.recursiveUpdate
          (self.lib.forEachDefaultSystem (
            { pkgs, ... }:
            {
              awx = pkgs.callPackage ./packages/awx.nix { };
              shantell-sans = pkgs.callPackage ./packages/shantell-sans.nix { };
            }
          ))
          (
            self.lib.forEachLinux (
              { pkgs, ... }:
              {
                lychee-slicer = pkgs.callPackage ./packages/lychee-slicer.nix { };
              }
            )
          );

      formatter = self.lib.forEachDefaultSystem ({ pkgs, ... }: pkgs.nixpkgs-fmt);

      # To get nixConfig in outputs too
      inherit (import ./flake.nix) nixConfig;
    };

  nixConfig = {
    extra-substituters = [ "http://contentment.bunny-lenok.ts.net:8069/default" ];
    extra-trusted-public-keys = [ "default:Eu4/pab8lXsIAeJxO+EULccfP/NDCWQAdlsbfwSKpFI=" ];
  };
}
