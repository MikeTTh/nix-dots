{ osImport
, userName
, homeDir
, ...
}:
{
  home.username = userName;
  home.homeDirectory = homeDir;

  home.stateVersion = "23.05";

  imports = [
    ./packages.nix
    ./guiPackages.nix
    ./settings
    ./services.nix
    ./devtools.nix
    osImport
  ];

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
