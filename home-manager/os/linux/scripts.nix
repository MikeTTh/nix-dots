{ pkgs, lib, ... }:
let
  podman = "${pkgs.podman}/bin/podman";
  ilofirefox = pkgs.writeScript "ilofirefox" ''
    #!/bin/sh

    if [ "$XAUTHORITY" = "" ] && [ -f "$HOME/.Xauthority" ]; then
      export XAUTHORITY="$HOME/.Xauthority"
    fi

    if [ "$XAUTHORITY" != "" ]; then
      xauthFlag="-v $XAUTHORITY:$XAUTHORITY:ro"
    fi

    ${podman} run -it --rm --user=firefox --network=host --ipc=host $xauthFlag -e "XAUTHORITY" -v /tmp/.X11-unix:/tmp/.X11-unix:ro -v $HOME/.local/share/ilofirefox:/home/firefox --userns keep-id -e "DISPLAY" --security-opt label=type:container_runtime_t robertvojcik/firefox-icedtea:iceweasel
  '';

  wf-recorder = lib.getExe pkgs.wf-recorder;
  mirror-screens = pkgs.writeScript "mirror-screens" ''
    #!/bin/sh
    ${wf-recorder} -c rawvideo -m sdl -f pipe:wayland-mirror
  '';
in
{
  home.packages = [
    (pkgs.runCommand "mike-scripts" { } ''
      mkdir -p $out/bin
      ln -s ${ilofirefox} $out/bin/ilofirefox
      ln -s ${mirror-screens} $out/bin/mirror-screens
    '')
  ];
}
