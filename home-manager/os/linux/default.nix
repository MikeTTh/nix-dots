{ ... }:
{
  imports = [
    ./guiPackages.nix
    ./cliPackages.nix
    ./services.nix
    ./settings.nix
    ./scripts.nix
    ./kde.nix
    ./flatpaks.nix
  ];
}
