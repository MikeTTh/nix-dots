{ config
, lib
, pkgs
, unstable
, flake
, inputs
, ...
}:
let
  lychee-slicer = flake.packages."${pkgs.system}".lychee-slicer;
in
{
  config = lib.mkIf config.hasGUI {
    home.packages = with pkgs; [
      # everyday things
      firefox # for fallback (if Flatpak were to fail)
      vlc
      obs-studio
      kate
      goldwarden
      pinentry-qt
      mangohud
      fragments
      unstable.audio-sharing
      unstable.mousam
      smile
      unstable.tidal-hifi
      gthumb

      # tools
      seabird
      gnome-disk-utility
      unstable.burpsuite
      qpwgraph
      filelight
      unstable.jetbrains-toolbox

      # productivity
      kdenlive
      mediainfo
      glaxnimate
      onlyoffice-bin
      libreoffice
      lychee-slicer
      (softmaker-office.override {
        officeVersion = {
          edition = "2021";
          version = "1068";
          hash = "sha256-pzgZyLiTIVIGQTWGFCM6pMiTPj4nBckT4LW5REX2RD0=";
        };
      })

      # games
      unstable.superTuxKart
      heroic
    ];

    nixpkgs.config.firefox = {
      speechSynthesisSupport = true;
      nativeMessagingHosts.packages = [ pkgs.plasma-browser-integration ];
    };
  };
}
