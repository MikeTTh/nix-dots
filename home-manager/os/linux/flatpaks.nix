{ config, inputs, lib, ... }:
{
  imports = [ inputs.declarative-flatpak.homeManagerModules.declarative-flatpak ];

  options.manageFlatpak =
    with lib;
    mkOption {
      type = types.bool;
      default = config.hasGUI;
      defaultText = literalExpression "config.hasGUI";
      description = "Declaratively manage Flatpaks";
      example = literalExpression "false";
    };

  config = lib.mkIf config.manageFlatpak {
    services.flatpak = {
      enableModule = true;
      remotes = {
        "flathub" = "https://dl.flathub.org/repo/flathub.flatpakrepo";
        "flathub-beta" = "https://dl.flathub.org/beta-repo/flathub-beta.flatpakrepo";
      };
      packages = [
        "flathub:app/app.zen_browser.zen//stable"
        "flathub:app/com.github.k4zmu2a.spacecadetpinball//stable"
        "flathub:app/io.mrarm.mcpelauncher//stable"
        "flathub:app/org.telegram.desktop//stable"
        "flathub:app/org.prismlauncher.PrismLauncher//stable"
        "flathub:app/org.gnome.Fractal//stable"
        "flathub:app/com.brave.Browser//stable"
        "flathub:app/eu.betterbird.Betterbird//stable"
        "flathub-beta:app/org.gimp.GIMP//beta" # GIMP 3 beta
      ];
      overrides = {
        "io.github.zen_browser.zen".environment."MOZ_ENABLE_WAYLAND" = "1";
      };
    };
  };
}
