{ pkgs, inputs, ... }:
{
  home.sessionVariablesExtra = ''
    export SSH_AUTH_SOCK="$HOME/Library/Group Containers/2BUA8C4S2C.com.1password/t/agent.sock"
  '';

  home.sessionVariables = {
    JAVA_HOME = "${pkgs.jdk}";
  };

  programs.go.goPrivate = [ "github.com/bitrise-io/*" ];

  # Replace aliases with app trampolines to make Spotlight work
  imports = [ inputs.mac-app-util.homeManagerModules.default ];
}
