{ inputs
, flake
, pkgs
, lib
, ...
}:
{
  nix = {
    # Here, we populate the Nix registry with some useful indirect inputs.
    # With this, we can run commands like `nix shell unstable#hello` or `nix shell nix-dots#awx`.
    # This also lets us pin the registry's nixpkgs to the rev specified in this flake.
    registry = {
      nix-dots.flake = flake;
      nixpkgs.flake = inputs.nixpkgs;
      pkgs.flake = inputs.nixpkgs;
      unstable.flake = inputs.nixpkgs-unstable;
      chaotic.flake = inputs.chaotic;
    };

    # And this fixes legacy nix commands, like `nix-shell` (which is not the flake-aware `nix shell`)
    settings.extra-nix-path = [ "nixpkgs=flake:nixpkgs" ];

    package = lib.mkDefault pkgs.nix;
  };

}
