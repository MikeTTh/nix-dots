{ lib
, config
, unstable
, pkgs
, ...
}:
{
  config = lib.mkIf config.hasGUI {
    programs.kitty = {
      enable = true;
      package = unstable.kitty;
      themeFile = "GitHub_Dark";
      font.name = "FiraCode Nerd Font";
      font.size = 11;
      settings = {
        cursor_shape = "beam";
        sync_to_monitor = "yes";
        reapint_delay = 6;
        input_delay = 1;
        visual_bell_duration = "0.3";
        macos_show_window_title_in = "window";
        wayland_titlebar_color = "background";
      };
    };

    home.packages = lib.mkIf pkgs.stdenv.isLinux [ unstable.ghostty ];
    xdg.configFile."ghostty/config".text = ''
      font-family = FiraCode Nerd Font
      font-size = 11
      theme = Argonaut
      cursor-style = bar
      link-url = true
      shell-integration-features = true
      gtk-single-instance = true
    '';
  };
}
