{ pkgs, ... }:
{
  programs.git = {
    enable = true;
    userName = "Miklós Tóth (Mike)";
    userEmail = "tothmiklostibor@gmail.com";
    lfs.enable = true;
    signing = {
      signByDefault = true;
      key = "~/.ssh/id_ed25519";
    };
    extraConfig = {
      gpg.format = "ssh";
      core = {
        autocrlf = "input";
        pager = "bat";
        repositoryFormatVersion = "1";
      };
      pull.rebase = "true";
      push.autoSetupRemote = "true";
      init.defaultBranch = "main";
      url = {
        "ssh://git@git.sch.bme.hu/" = {
          insteadOf = "https://git.sch.bme.hu/";
        };
        "ssh://git@github.com/" = {
          insteadOf = "https://github.com/";
        };
      };
      extensions = {
        objectFormat = "sha256";
        compatObjectFormat = "sha1";
      };
    };
    includes = [
      {
        path = (pkgs.formats.ini { }).generate "bitrise.gitconfig" {
          user.email = "miklos.toth@bitrise.io";
        };
        condition = "gitdir:~/Code/bitrise/**";
      }
    ];
  };
}
