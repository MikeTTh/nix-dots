{ pkgs
, lib
, inputs
, ...
}:
{
  programs.zsh = {
    enable = true;
    dotDir = ".config/zsh";
    autosuggestion.enable = true;
    syntaxHighlighting.enable = true;
    enableCompletion = true;
    enableVteIntegration = true;
    historySubstringSearch = {
      enable = true;
    };
    oh-my-zsh = {
      enable = true;
      plugins = [
        "git"
        "colored-man-pages"
        "command-not-found"
        "golang"
        "ansible"
        "kubectl"
        "helm"
        "terraform"
      ];
      theme = "";
    };
    shellAliases = {
      cat = "bat -p";
      less = "less -r";
      ssh = "TERM=xterm-256color ssh";
      pkg = "NIXPKGS_ALLOW_UNFREE=1 nix shell --impure";
      gcloud = "TERM=xterm-256color gcloud";
      pip = "uv pip";
      sudo = "TERM=xterm-256color sudo";
    };
    plugins = [
      {
        name = "powerlevel10k";
        src = pkgs.zsh-powerlevel10k;
        file = "share/zsh-powerlevel10k/powerlevel10k.zsh-theme";
      }
      {
        name = "powerlevel10k-config";
        src = lib.cleanSource ../files;
        file = "p10k.zsh";
      }
    ];
    envExtra = ''
      export PATH="$HOME/bin:$PATH:$GOBIN"

      function _set_kubeconfig() {
          local files=(~/.kube/*.yaml(N))
          export KUBECONFIG="$HOME/.kube/config:''${(j/:/)files}"
      }

      _set_kubeconfig

      eval "$(${lib.getExe pkgs.pay-respects} zsh --alias)"
    '';
  };

  programs.mcfly = {
    enable = true;
    enableZshIntegration = true;
    fuzzySearchFactor = 2;
  };

  programs.zoxide = {
    enable = true;
    enableZshIntegration = true;
    options = [
      "--cmd"
      "cd"
    ];
  };

  home.sessionVariables = {
    MCFLY_RESULTS = "50";
  };

  # Fix command-not-found on Flake-based systems
  imports = [ inputs.nix-index-database.hmModules.nix-index ];
}
