{ ... }:
let
  deprecatedSecurity = {
    KexAlgorithms = "+diffie-hellman-group1-sha1,diffie-hellman-group-exchange-sha1,diffie-hellman-group14-sha1";
    Ciphers = "+3des-cbc,aes128-cbc,aes192-cbc,aes256-cbc,aes128-ctr,aes192-ctr,aes256-ctr";
    HostKeyAlgorithms = "+ssh-rsa";
  };
in
{
  programs.ssh = {
    enable = true;

    compression = true;
    controlMaster = "auto";
    controlPersist = "10m";
    controlPath = "~/.ssh/socket/%r@%h:%p";
    serverAliveInterval = 10;
    addKeysToAgent = "yes";

    extraConfig = ''
      LogLevel DEBUG
      ConnectTimeout 30
    '';

    matchBlocks = {
      # own stuff
      "contentment" = {
        hostname = "contentment.bunny-lenok.ts.net";
        user = "root";
      };
      "szappantarto" = {
        hostname = "192.168.0.253";
        user = "root";
        extraOptions = deprecatedSecurity;
      };

      # KSZK
      "db-sch".hostname = "10.151.204.67";
      "noc-jump".hostname = "10.151.200.100";
      "noc-a" = {
        hostname = "noc-a.sch.bme.hu";
        port = 10022;
      };
      "noc-b" = {
        hostname = "noc-b.sch.bme.hu";
        port = 10022;
      };
      "sw-* rtr-1" = {
        proxyJump = "noc-jump";
        extraOptions = deprecatedSecurity;
      };
      "trash trash.sch.bme.hu" = {
        hostname = "trash.sch.bme.hu";
        port = 10022;
      };
      "beholder".hostname = "beholder.sch.bme.hu";
      "cctv".hostname = "cctv.sch.bme.hu";
    };
  };

  home.file.".ssh/socket/.keep".text = "";
}
