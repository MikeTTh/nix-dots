{ pkgs
, lib
, flake
, ...
}:
{
  environment.packages = with pkgs; [
    nano
    procps
    killall
    findutils
    utillinux
    tzdata
    hostname
    man
    coreutils
    gzip
    pigz
    zip
    unzip
  ];

  user.shell = lib.getExe pkgs.zsh;

  environment.etcBackupExtension = ".bak";
  environment.etc."resolv.conf".text = lib.mkForce ''
    nameserver 100.100.100.100
    nameserver 1.1.1.1
    nameserver 8.8.8.8
  '';
  system.stateVersion = "24.05";
  nix = {
    substituters = flake.nixConfig.extra-substituters;
    trustedPublicKeys = flake.nixConfig.extra-trusted-public-keys;
    package = pkgs.lix;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };
  time.timeZone = "Europe/Budapest";
}
