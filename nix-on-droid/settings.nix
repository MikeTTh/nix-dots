{ pkgs, ... }:
{
  terminal.font = "${pkgs.fira-code-nerdfont}/share/fonts/truetype/NerdFonts/FiraCodeNerdFont-Retina.ttf";
  terminal.colors = {
    background = "#1e1e1e";
    foreground = "#cccccc";
    cursor = "#ffffff";

    # black
    color0 = "#000000";
    color8 = "#666666";

    # red
    color1 = "#f14c4c";
    color9 = "#cd3131";

    # green
    color2 = "#23d18b";
    color10 = "#0dbc79";

    # yellow
    color3 = "#f5f543";
    color11 = "#e5e510";

    # blue
    color4 = "#3b8eea";
    color12 = "#2472c8";

    # magenta
    color5 = "#d670d6";
    color13 = "#bc3fbc";

    # cyan
    color6 = "#29b8db";
    color14 = "#11a8cd";

    # white
    color7 = "#e5e5e5";
    color15 = "#e5e5e5";
  };
}
