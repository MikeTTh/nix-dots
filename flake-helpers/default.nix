self:
let
  forEach = import ./forEach.nix self;
  generators = import ./generators.nix self;
in
forEach // generators
