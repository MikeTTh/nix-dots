self: {
  generateHMConfig = (
    { system
    , osImport
    , userName
    , homeDir
    ,
    }:
    self.inputs.home-manager.lib.homeManagerConfiguration {
      pkgs = self.inputs.nixpkgs.legacyPackages."${system}";
      modules = [ ../home-manager ];
      extraSpecialArgs = {
        flake = self;
        inputs = self.inputs;
        unstable = import self.inputs.nixpkgs-unstable {
          system = system;
          config.allowUnfree = true;
        };
        inherit osImport userName homeDir;
      };
    }
  );
}
