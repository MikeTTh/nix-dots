{ ... }:
{
  services.pipewire.extraConfig.pipewire."69-rates" = {
    "context.properties" = {
      "default.clock.allowed-rates" = [
        44100
        48000
        88200
        96000
        176400
        192000
      ];
      "default.clock.rate" = 192000;
    };
  };
  services.pipewire.wireplumber.extraConfig."69-bluetooth-rates" = {
    "monitor.bluez.properties" = {
      "bluez5.default.rate" = 96000;
    };
  };
}
