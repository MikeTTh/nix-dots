{ pkgs, flake, lib, ... }:
{
  services.xserver.enable = true;

  services.displayManager.sddm.enable = true;
  services.displayManager.sddm.wayland.enable = true;
  services.desktopManager.plasma6.enable = true;

  # Fix kwallet unlock on startup
  systemd.services.display-manager = {
    serviceConfig = {
      KeyringMode = "inherit";
    };
  };
  security.pam.services.sddm-autologin.text = lib.mkForce ''
    auth     requisite pam_nologin.so
    auth     required  pam_succeed_if.so uid >= 1000 quiet
    auth     required  pam_permit.so

    auth    optional    ${pkgs.systemd}/lib/security/pam_systemd_loadkey.so
    auth    required    pam_unix.so likeauth
    auth    optional    ${pkgs.kdePackages.kwallet-pam}/lib/security/pam_kwallet5.so

    account  include   sddm

    password include   sddm

    session  include   sddm
  '';

  # https://nixos.wiki/wiki/GNOME#automatic_login
  services.displayManager.autoLogin.enable = true;
  services.displayManager.autoLogin.user = "mike";
  systemd.services."getty@tty1".enable = false;
  systemd.services."autovt@tty1".enable = false;

  services.geoclue2 = {
    enable = true;
    enableWifi = true;

    enableNmea = false;
    enable3G = false;
    enableCDMA = false;
    enableModemGPS = false;
  };

  xdg.portal = {
    enable = true;
    extraPortals = [ ];
  };

  fonts = {
    packages = with pkgs; [
      noto-fonts
      noto-fonts-cjk-sans
      noto-fonts-emoji
      font-awesome
      fira-code-nerdfont
      source-han-sans
      source-han-sans-japanese
      source-han-serif-japanese
      flake.packages."${system}".shantell-sans
    ];
    fontconfig.defaultFonts = {
      serif = [
        "Noto Serif"
        "Source Han Serif"
        "Noto Color Emoji"
      ];
      sansSerif = [
        "Noto Sans"
        "Source Han Sans"
        "Noto Color Emoji"
      ];
      monospace = [ "FiraCode Nerd Font" ];
      emoji = [ "Noto Color Emoji" ];
    };
    fontconfig.useEmbeddedBitmaps = true;
  };

  # Fix GTK on non-Gnome Wayland
  programs.dconf.enable = true;

  services.logind = {
    killUserProcesses = true;
  };

  programs.kdeconnect.enable = true;

  environment.systemPackages = with pkgs; [ kdePackages.plasma-thunderbolt ];
}
