{ unstable, lib, ... }:
rec {
  networking.useDHCP = lib.mkDefault true;

  networking.networkmanager = {
    enable = true;
    enableStrongSwan = true;
    wifi.backend = "iwd";
  };
  networking.nftables.enable = true;
  services.tailscale = {
    enable = true;
    useRoutingFeatures = "client";
    package = unstable.tailscale;
  };

  networking.nameservers = [
    "1.1.1.1#one.one.one.one"
    "1.0.0.1#one.one.one.one"
    "9.9.9.9#dns.quad9.net"
  ];

  services.resolved = {
    enable = true;
    fallbackDns = networking.nameservers;
    dnsovertls = "opportunistic";
    llmnr = "true";
    dnssec = "allow-downgrade";
    domains = [ "~." ];
    extraConfig = ''
      DNSStubListenerExtra=198.51.100.0
      MulticastDNS=true
      Cache=no-negative
    '';
  };

  networking.firewall = {
    enable = true;
    allowedTCPPorts = [
      22000 # syncthing
    ];
    allowedUDPPorts = [
      22000 # syncthing (QUIC)
    ];
    trustedInterfaces = [
      "nonexistant0"
      "virbr0"
    ];
  };

  networking.interfaces.nonexistant0 = {
    virtual = true;
    ipv4.addresses = [
      {
        address = "198.51.100.0";
        prefixLength = 32;
      }
    ];
  };

  networking.extraHosts = ''
    127.0.0.1 localghost xn--9q8h xn--local-f604d
  '';

  systemd.services.NetworkManager-wait-online.enable = false;
}
