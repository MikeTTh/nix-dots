{ config
, inputs
, pkgs
, ...
}:
{
  imports = [ inputs.chaotic.nixosModules.default ];

  boot.initrd.systemd.enable = true;
  #boot.extraModulePackages = with config.boot.kernelPackages; [ v4l2loopback ]; TODO reenable after 6.13 update is done

  boot.plymouth.enable = true;
  boot.kernelParams = [
    "quiet"
    "preempt=full"
  ];

  security.unprivilegedUsernsClone = true;
  security.allowUserNamespaces = true;

  systemd.services.dev-chmod = {
    enable = true;
    description = "set permissions of devices";
    script = ''
      echo Setting permissions of /dev/uhid
      chgrp input /dev/uhid
      chmod 0660 /dev/uhid
    '';
    serviceConfig = {
      Type = "oneshot";
    };
    wantedBy = [ "multi-user.target" ];
  };

  services.dbus.implementation = "broker";

  services.udev.extraRules = ''
    KERNEL=="uhid", MODE="0660", GROUP="input"
  '';

  programs.nh = {
    enable = true;
    package = inputs.tmp-nh-master.packages."${pkgs.system}".nh;
    clean = {
      enable = true;
      extraArgs = "--keep-since 7d --keep 3";
      dates = "weekly";
    };
    flake = "/home/mike/Code/me/nix-dots";
  };

  systemd = {
    extraConfig = "DefaultTimeoutStopSec=10s";
    user.extraConfig = "DefaultTimeoutStopSec=10s";
  };
}
