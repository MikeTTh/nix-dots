{ config, pkgs, ... }:
{
  services.openssh = {
    enable = true;
    startWhenNeeded = true;
  };
  services.hardware.bolt.enable = true;
  services.flatpak.enable = true;

  system.fsPackages = [ pkgs.fuse-overlayfs ];
  virtualisation = {
    podman = {
      enable = true;
      defaultNetwork.settings = {
        dns_enabled = true;
      };
    };
    docker.rootless = {
      enable = true;
      setSocketVariable = true;
      daemon.settings = {
        dns = [ "198.51.100.0" ];
        ipv6 = true;
        fixed-cidr-v6 = "fd00::/80";
        storage-driver = "fuse-overlayfs";
      };
    };
    waydroid.enable = true;
    libvirtd = {
      enable = true;
      qemu.swtpm.enable = true;
    };
    spiceUSBRedirection.enable = true;
    vmware.host = {
      enable = false;
      package = pkgs.vmware-workstation;
      extraPackages = [ config.boot.kernelPackages.vmware ];
    };
  };
}
