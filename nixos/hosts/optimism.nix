{ pkgs, inputs, ... }:
{
  networking.hostName = "optimism";
  imports = [ inputs.nixos-hardware.nixosModules.framework-13-7040-amd ];
  boot.kernelPackages = pkgs.linuxPackages_cachyos;
  services.scx = {
    enable = true;
    scheduler = "scx_lavd";
    extraArgs = [ ];
  };

  boot.kernel.sysctl = {
    "vm.page_lock_unfairness" = 1;
    "vm.watermark_boost_factor" = 1;
    "vm.min_free_kbytes" = 1048576;
    "vm.watermark_scale_factor" = 500;
  };
  boot.kernelModules = [ "ntsync" ];

  # not needed above Linux 6.12
  hardware.framework.enableKmod = false;

  hardware.amdgpu.opencl.enable = true;
  environment.sessionVariables = {
    RADV_PERFTEST = "sam,gpl,rt,nggc,transfer_queue,video_decode";
    VKD3D_CONFIG = "dxr,dxr11";
    AMD_USERQ = "1";
    RADV_FORCE_VRS = "2x2";
    DXVK_ASYNC = "1";
  };

  chaotic.mesa-git = {
    enable = false;
    extraPackages = with pkgs; [ rocmPackages.clr.icd ];
    extraPackages32 = with pkgs; [ rocmPackages.clr.icd ];
  };

  hardware.framework.laptop13.audioEnhancement.enable = true;

  environment.systemPackages = with pkgs; [
    amdgpu_top
    fw-ectool
  ];

  nixpkgs.config.rocmSupport = true;
}
