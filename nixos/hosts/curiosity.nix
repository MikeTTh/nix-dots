{ pkgs, ... }:
{
  imports = [ <nixos-hardware/lenovo/thinkpad/t480> ];

  services.throttled.extraConfig = ''
    [GENERAL]
    Enabled: True
    Sysfs_Power_Path: /sys/class/power_supply/AC*/online
    Autoreload: True

    [BATTERY]
    Update_Rate_s: 30
    PL1_Tdp_W: 29
    PL1_Duration_s: 28
    PL2_Tdp_W: 44
    PL2_Duration_S: 0.002
    Trip_Temp_C: 85
    cTDP: 0

    [AC]
    Update_Rate_s: 5
    PL1_Tdp_W: 44
    PL1_Duration_s: 28
    PL2_Tdp_W: 44
    PL2_Duration_S: 0.002
    Trip_Temp_C: 95
    HWP_Mode: True
    cTDP: 2

    [UNDERVOLT.BATTERY]
    CORE: -80
    GPU: -60
    CACHE: -80
    UNCORE: -60
    ANALOGIO: 0

    [UNDERVOLT.AC]
    CORE: -50
    GPU: -40
    CACHE: -50
    UNCORE: -40
    ANALOGIO: 0

  '';

  environment.systemPackages = with pkgs; [ intel-gpu-tools ];
}
