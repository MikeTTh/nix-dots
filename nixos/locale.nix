{ ... }:
{
  time.timeZone = "Europe/Budapest";

  i18n.defaultLocale = "en_GB.UTF-8";

  console.keyMap = "uk";
}
