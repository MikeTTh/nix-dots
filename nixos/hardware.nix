{ pkgs, ... }:
{
  services.fwupd.enable = true;
  hardware.bluetooth = {
    enable = true;
    settings = {
      General = {
        Experimental = true;
      };
    };
  };
  hardware.enableRedistributableFirmware = true;

  services.printing = {
    enable = true;
    drivers = with pkgs; [ brlaser gutenprint ];
    startWhenNeeded = true;
    cups-pdf.enable = true;
  };

  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
    wireplumber.enable = true;
  };

  environment.systemPackages = with pkgs; [
    tpm2-tools
    vulkan-validation-layers
  ];

  # enable sysrq
  boot.kernel.sysctl = {
    "kernel.sysrq" = 1;
    "net.core.rmem_max" = 7500000;
    "net.core.wmem_max" = 7500000;
    "vm.max_map_count" = 2147483642; # https://wiki.archlinux.org/title/Gaming#Increase_vm.max_map_count
  };

  security.tpm2.enable = true;
  security.tpm2.pkcs11.enable = true;
  security.tpm2.tctiEnvironment.enable = true;
  users.users.mike.extraGroups = [ "tss" ];

  # enable Solaar and relevant udev rules
  hardware.logitech.wireless = {
    enable = true;
    enableGraphical = true;
  };

  systemd.services.logid =
    let
      config = pkgs.writeText "logid.cfg" ''
        devices: ({
          name: "Wireless Mouse MX Master 3";
          smartshift: { on: true; threshold: 20; };
          hiresscroll: { hires: true; invert: false; target: false; };
          dpi: 1500;
        });
      '';
    in
    {
      enable = true;
      description = "Logitech config daemon";
      serviceConfig = {
        Type = "simple";
        ExecStart = "${pkgs.logiops}/bin/logid -c ${config}";
      };
      wantedBy = [ "multi-user.target" ];
    };

  boot.kernelModules = [ "i2c-dev" ];
  services.udev.extraRules = ''
    KERNEL=="i2c-*", MODE="0660", GROUP="input"
  '';

  # Enable pstore (for storing crash logs)
  boot.initrd.availableKernelModules = [ "pstore" "efi_pstore" ];
  boot.initrd.kernelModules = [ "pstore" "efi_pstore" ];
  boot.kernelParams = [ "efi_pstore.pstore_disable=0" ];
}
