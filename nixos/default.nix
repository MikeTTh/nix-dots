{ ... }:
{
  imports = [
    ./system.nix
    ./packages.nix
    ./services.nix
    ./hardware.nix
    ./audio-tricks.nix
    ./networking.nix
    ./locale.nix
    ./gui.nix
    ./me.nix
    ./misc-config.nix
    ./magic.nix
    ./binfmt.nix
    ./home-manager.nix
    ./secure-boot.nix
    ./usbguard.nix
  ];

  system.stateVersion = "23.05";
}
