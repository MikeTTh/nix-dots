{ config
, pkgs
, lib
, ...
}:
{
  programs.ydotool.enable = true;

  # Make /bin/bash available for non-Nix-aware programs
  systemd.tmpfiles.rules = [
    "L+ /bin/bash - - - - ${lib.getExe pkgs.bash}"
  ];

  # Make non-Nix-aware programs work with Nix
  programs.nix-ld = {
    enable = true;
    libraries =
      (pkgs.appimageTools.defaultFhsEnvArgs.targetPkgs pkgs)
      ++ (pkgs.appimageTools.defaultFhsEnvArgs.multiPkgs pkgs);
  };
  # nix-ld places env vars in /etc/profile, let's fix that
  environment.sessionVariables."NIX_LD" = "/run/current-system/sw/share/nix-ld/lib/ld.so";
  environment.sessionVariables."NIX_LD_LIBRARY_PATH" = "/run/current-system/sw/share/nix-ld/lib";

  programs.appimage = {
    enable = true;
    binfmt = true;
  };

  # Magic FS that follows symlinks to make flatpak host icons/fonts work
  system.fsPackages = [ pkgs.bindfs ];
  fileSystems =
    let
      mkRoSymBind = path: {
        device = path;
        fsType = "fuse.bindfs";
        options = [
          "ro"
          "resolve-symlinks"
          "x-gvfs-hide"
        ];
      };
      aggregatedFonts = pkgs.buildEnv {
        name = "system-fonts";
        paths = config.fonts.packages;
        pathsToLink = [ "/share/fonts" ];
      };
    in
    {
      # Create an FHS mount to support flatpak host icons/fonts
      "/usr/share/icons" = mkRoSymBind (config.system.path + "/share/icons");
      "/usr/share/fonts" = mkRoSymBind (aggregatedFonts + "/share/fonts");
    };

  services.udev.extraRules = ''
    #Flipper Zero serial port
    SUBSYSTEMS=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="5740", ATTRS{manufacturer}=="Flipper Devices Inc.", TAG+="uaccess", GROUP="dialout"
    #Flipper Zero DFU
    SUBSYSTEMS=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="df11", ATTRS{manufacturer}=="STMicroelectronics", TAG+="uaccess", GROUP="dialout"
    #Flipper ESP32s2 BlackMagic
    SUBSYSTEMS=="usb", ATTRS{idVendor}=="303a", ATTRS{idProduct}=="40??", ATTRS{manufacturer}=="Flipper Devices Inc.", TAG+="uaccess", GROUP="dialout"
    #Flipper U2F
    SUBSYSTEMS=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="5741", ATTRS{manufacturer}=="Flipper Devices Inc.", ENV{ID_SECURITY_TOKEN}="1"
    # NTSync
    KERNEL=="ntsync", MODE="0644"
  '';
}
