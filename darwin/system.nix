{ pkgs
, flake
, inputs
, ...
}:
{
  imports = [ (inputs.tmp-nh-darwin-pr + "/modules/programs/nh.nix") ];

  environment.systemPackages = with pkgs; [
    redis
    htop
    procps
    darwin.iproute2mac
    mtr
    keka
  ];

  nixpkgs.config.allowUnfree = true;

  security.pam.enableSudoTouchIdAuth = true;

  # Auto upgrade nix package and the daemon service.
  services.nix-daemon.enable = true;
  nix = {
    settings = {
      experimental-features = [
        "nix-command"
        "flakes"
      ];
      substituters = flake.nixConfig.extra-substituters;
      trusted-public-keys = flake.nixConfig.extra-trusted-public-keys;
    };
    package = pkgs.lix;
    # Auto-optimise is broken on Darwin: https://github.com/NixOS/nix/issues/7273
    optimise = {
      automatic = true;
      # Saturday 00:00
      interval = [
        {
          Weekday = 7;
          Hour = 0;
          Minute = 0;
        }
      ];
      user = "miklos.toth";
    };
  };

  programs.nh = {
    enable = true;
    package = inputs.tmp-nh-master.packages."${pkgs.system}".nh;
    clean = {
      enable = true;
      extraArgs = "--keep-since 7d --keep 3";
    };
  };

  # Create /etc/zshrc that loads the nix-darwin environment.
  programs.zsh.enable = true;

  system.stateVersion = 4;
}
